package com.t1.alieva.tm.service;

import com.t1.alieva.tm.api.ICommandRepository;
import com.t1.alieva.tm.api.ICommandService;
import com.t1.alieva.tm.model.Command;

public class CommandService implements ICommandService {
    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }
}
