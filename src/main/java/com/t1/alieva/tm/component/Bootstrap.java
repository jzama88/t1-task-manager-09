package com.t1.alieva.tm.component;

import com.t1.alieva.tm.api.ICommandController;
import com.t1.alieva.tm.api.ICommandRepository;
import com.t1.alieva.tm.api.ICommandService;
import com.t1.alieva.tm.constant.ArgumentConst;
import com.t1.alieva.tm.constant.TerminalConst;
import com.t1.alieva.tm.controller.CommandController;
import com.t1.alieva.tm.repository.CommandRepository;
import com.t1.alieva.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {
    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private void processCommand(final String command) {
        if (command == null) {
            commandController.showErrorCommand();
            return;
        }
        switch (command) {
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.EXIT:
                exit();
            default:
                commandController.showErrorCommand();
        }
    }

    private void processArgument(final String argument) {
        if (argument == null) {
            commandController.showErrorArgument();
            return;
        }
        switch (argument) {
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                commandController.showErrorArgument();
        }
    }

    private void exit() {
        System.exit(0);
    }
    public void run (final String[] args){
        if (processArguments(args)) System.exit(0);
        commandController.showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }
    private boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;

        final String arg = args[0];
        processArgument(arg);
        return true;
    }
}
