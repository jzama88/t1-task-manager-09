package com.t1.alieva.tm.repository;

import com.t1.alieva.tm.api.ICommandRepository;
import com.t1.alieva.tm.constant.ArgumentConst;
import com.t1.alieva.tm.constant.TerminalConst;
import com.t1.alieva.tm.model.Command;

public class CommandRepository implements ICommandRepository {
    public static Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Show developer info."
    );

    public static Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Show application version."
    );

    public static Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Show application commands."
    );

    public static Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close application"
    );
    public static Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Show information about device"
    );
    private static Command[] TERMINAL_COMMANDS = new Command[] {
            INFO, ABOUT, VERSION, HELP, EXIT
    };
    @Override
    public Command[] getTerminalCommands(){
        return TERMINAL_COMMANDS;
    }
}
